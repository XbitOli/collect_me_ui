using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ButtonActivity : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Image _image;
    [SerializeField] private TextMeshProUGUI _textMeshPro;
    [SerializeField] private float _speedTextChange = 5f;


    private Color _defaultColor;
    private Color _colorStartMeshText;

    private Color _buttonClick = new(109, 156, 0, 255);
    private const float _alphaOnButtonEnter = 255f;

    private Transform _currentTransform;
    private Coroutine _scaleChanger;


    private void Awake()
    { 
        _defaultColor = _image.color;
        _colorStartMeshText = _textMeshPro.color;
    }

    private void Start()
    {
        _currentTransform = gameObject.GetComponent<Transform>();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(_scaleChanger != null)
            StopCoroutine(_scaleChanger);

        _scaleChanger = StartCoroutine(IncreaseSize(1.1f));

        _image.color = new Color(_defaultColor.r, _defaultColor.g, _defaultColor.b, _alphaOnButtonEnter);
        _textMeshPro.color = new Color(_colorStartMeshText.r, _colorStartMeshText.g, _colorStartMeshText.b, _alphaOnButtonEnter);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(_scaleChanger != null)
            StopCoroutine(_scaleChanger);

        _scaleChanger = StartCoroutine(DecreaseSize(1.0f));

        _image.color = _defaultColor;
        _textMeshPro.color = _colorStartMeshText;
    }

    private IEnumerator IncreaseSize(float size)
    {
        for (int i = 0; _currentTransform.localScale.x < size &&
                        _currentTransform.localScale.y < size; i++)
        {
            float x = Mathf.Lerp(_currentTransform.localScale.x, size, _speedTextChange * Time.deltaTime);
            float y = Mathf.Lerp(_currentTransform.localScale.y, size, _speedTextChange * Time.deltaTime);

            _currentTransform.localScale = new Vector3(x, y, 1);

            yield return null;
        }
    }

    private IEnumerator DecreaseSize(float size)
    {
        for (int i = 0; _currentTransform.localScale.x > size &&
                        _currentTransform.localScale.y > size; i++)
        {
            float x = Mathf.Lerp(_currentTransform.localScale.x, size, _speedTextChange * Time.deltaTime);
            float y = Mathf.Lerp(_currentTransform.localScale.y, size, _speedTextChange * Time.deltaTime);

            _currentTransform.localScale = new Vector3(x, y, 1);

            yield return null;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _image.color = _buttonClick;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _image.color = new Color(_defaultColor.r, _defaultColor.g, _defaultColor.b, _alphaOnButtonEnter);
    }
}
